# Koriym.SunDb

Pulling database values makes tests harder.

Instead, Let's *pass* them into invoke method parameter.


```php
$injector = new Injector(new SunDbModule($sqlDir, $dsn, $user, $pass));
$foo = $injector->getInstance(Foo::class);
$task = $foo->getTask(1);
```

`sql/task_item.sql`

```
select * from task where id = :id;
```

You can can recieve an database query result in various way with `@Assist` and `@Sql` annotation.

```php
use Koriym/SunDb/Annotation/Sql;
use Ray/Di/Di/Assist;

class Foo
{
    /**
     * As an invokable statement
     * 
     * @Assist({"sth"})
     * @Sql("task_item")
     */
    public function getTaskByCallable($id, SthInterface $sth)
    {
        $task = $sth(['id' => (int) $id]);

        return $task;
    }
    
    // or
    
    /**
     * As a PDOStatement
     * 
     * @Assist({"sth"})
     * @Sql("task_item")
     */
    public function getTaskByStmt($id, PDOStatement $sth)
    {
        $sth->bindParam('id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $task = $sth->fetch(PDO::FETCH_ASSOC);

        return $task;
    }

    // or
    
    /**
     * As a query result
     * 
     * @Assist({"task"})
     * @Sql("task_item")
     */
    public function getTaskByDbValue($id, DbValue $task)
    {
        return (array) $task;
    }
}
```

## Test

```
$foo = new Foo;
$foo->getTask(1, new Sth(function ($id) { return ['title' => 'shopping']});
```