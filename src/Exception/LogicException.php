<?php
/**
 * This file is part of the Koriym.SunDb
 *
 * @license http://opensource.org/licenses/MIT MIT
 */
namespace Koriym\SunDb\Exception;

class LogicException extends \LogicException implements ExceptionInterface
{
}
