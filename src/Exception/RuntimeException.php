<?php
/**
 * This file is part of the Koriym.SunDb
 *
 * @license http://opensource.org/licenses/MIT MIT
 */
namespace Koriym\SunDb\Exception;

class RuntimeException extends \RuntimeException implements ExceptionInterface
{
}
